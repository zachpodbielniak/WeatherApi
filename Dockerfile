FROM tomcat:9-jdk11-openjdk

WORKDIR /usr/local/tomcat/webapps
COPY ./target/Weather.war .

EXPOSE 8080
CMD ["/usr/local/tomcat/bin/catalina.sh", "run"]