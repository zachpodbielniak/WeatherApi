package net.libpod.weather;
import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/CurrentForecast")
public class CurrentForecast extends HttpServlet {
	
	private static final long serialVersionUID = 1L;


	public CurrentForecast() 
	{ super(); }
	

	protected void doGet(
		HttpServletRequest 	request,
		HttpServletResponse 	response
	) throws ServletException, IOException
	{
		String sResponse = "";
		String sFormat = "";
		boolean bDetailed = false;
		Enumeration<String> eParams = request.getParameterNames();
		HourlyForecastRunner hfrRunner = HourlyForecastRunner.getSingleton();


		/* Enumerate over query string */
		if (null != eParams)
		{
			while (eParams.hasMoreElements())
			{
				String sNextParam = eParams.nextElement();
				
				if (sNextParam.equalsIgnoreCase("format"))
				{ sFormat = request.getParameter(sNextParam); }
				else if (sNextParam.equalsIgnoreCase("detailed"))
				{ bDetailed = Boolean.parseBoolean(request.getParameter(sNextParam)); }
			}
		}

		if (sFormat.equalsIgnoreCase("json"))
		{
			response.setContentType("application/json;charset=utf-8");
			sResponse = "{\"short\": ";
			sResponse += hfrRunner.sShortForecast;
			sResponse += ",\"detailed\": \"";
			sResponse += hfrRunner.sDetailedForecast;
			sResponse += "\"}";
		}
		else 
		{
			response.setContentType("text/plain;charset=utf-8");
			if (true == bDetailed)
			{ sResponse = hfrRunner.sDetailedForecast; } 
			else 
			{ sResponse = hfrRunner.sShortForecast; }
		}

		response.getWriter().append(sResponse);
	}
	
}