package net.libpod.weather;


import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.text.StyledEditorKit;

import net.libpod.weather.AppStartup;
import net.libpod.weather.HourlyForecastRunner;

@WebServlet("/Update")
public class Update extends HttpServlet {

	private static final long serialVersionUID = 1L;


	public Update() 
	{ super(); }
	

	protected void doGet(
		HttpServletRequest 	request,
		HttpServletResponse 	response
	) throws ServletException, IOException
	{
		String sResponse = "";
		String sFormat = "";
		String sType = "all";
		String sStatus = "";
		boolean bDetailed = false;
		Enumeration<String> eParams = request.getParameterNames();
		HourlyForecastRunner hfrRunner = HourlyForecastRunner.getSingleton();


		/* Enumerate over query string */
		if (null != eParams)
		{
			while (eParams.hasMoreElements())
			{
				String sNextParam = eParams.nextElement();
				
				if (sNextParam.equalsIgnoreCase("format"))
				{ sFormat = request.getParameter(sNextParam); }
				else if (sNextParam.equalsIgnoreCase("detailed"))
				{ bDetailed = Boolean.parseBoolean(request.getParameter(sNextParam)); }
				else if (sNextParam.equalsIgnoreCase("type"))
				{ sType = request.getParameter(sNextParam); }
			}
		}

		if (sType.equalsIgnoreCase("all")) {
			HourlyForecastRunner.getSingleton().doRun();
			sStatus = "OK";
		} else if (sType.equalsIgnoreCase("hourly")) {
			HourlyForecastRunner.getSingleton().doRun();
			sStatus = "OK";
		} else {
			sStatus = "ERROR! type=" + sType + " is not a valid type!\n";
			response.setStatus(400);
		}

		if (sFormat.equalsIgnoreCase("json"))
		{
			response.setContentType("application/json;charset=utf-8");
			sResponse = "{\"status\": ";
			sResponse += sStatus;
			sResponse += "\"}";
		}
		else 
		{
			response.setContentType("text/plain;charset=utf-8");
			sResponse = sStatus;
		}

		response.getWriter().append(sResponse);
	}
	
}
