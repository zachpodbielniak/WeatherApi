package net.libpod.weather;

import java.util.Date;
import java.util.Calendar;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.concurrent.TimeUnit;
import java.io.IOException;
import java.lang.InterruptedException;

import net.libpod.weather.HourlyForecastRunner;

public class BackgroundWorkerThread extends Thread
{
	private DateFormat df;

	public BackgroundWorkerThread()
	{ 
		super(); 
		df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	}

	private void getData() throws InterruptedException
	{
		try 
		{ 
			System.err.println(this.df.format(Calendar.getInstance().getTime()) + " [" + Thread.currentThread().getName() + "] Getting Data: " + new Date());
			HourlyForecastRunner hfrInstance;
			hfrInstance = HourlyForecastRunner.getSingleton(); 

			/* Perform the run of data */
			hfrInstance.doRun();

			/* Log current state */
			System.err.println(this.df.format(Calendar.getInstance().getTime()) + " [" + Thread.currentThread().getName() + "] Temperature: " + hfrInstance.sTemperature);
			System.err.println(this.df.format(Calendar.getInstance().getTime()) + " [" + Thread.currentThread().getName() + "] WindSpeed: " + hfrInstance.sWindSpeed);
			System.err.println(this.df.format(Calendar.getInstance().getTime()) + " [" + Thread.currentThread().getName() + "] WindDirection: " + hfrInstance.sWindSpeedDirection);
			System.err.println(this.df.format(Calendar.getInstance().getTime()) + " [" + Thread.currentThread().getName() + "] ShortForecast: " + hfrInstance.sShortForecast);
			System.err.println(this.df.format(Calendar.getInstance().getTime()) + " [" + Thread.currentThread().getName() + "] Processed Data: " + new Date());
		}
		catch (IOException exception)
		{ System.err.print(exception); }
	}

	public void runOnce()
	{
		System.err.println(this.df.format(Calendar.getInstance().getTime()) + " [" + Thread.currentThread().getName() + "] Started");

		try
		{ getData(); }
		catch (InterruptedException exception)
		{ System.err.println(exception.getMessage()); }

		System.err.println(this.df.format(Calendar.getInstance().getTime()) + " [" + Thread.currentThread().getName() + "] Ended");
	}


	@Override
	public void run()
	{
		for (;;)
		{
			try
			{ Thread.sleep(300000); }
			catch (InterruptedException exception)
			{ System.err.println(exception.getMessage()); }
			
			runOnce(); 
		}
	}
}