package net.libpod.weather;

import java.net.http.*;
import java.io.IOException;
import java.net.URI;
import org.json.*;

public class HourlyForecastRunner {

	private static HourlyForecastRunner hfcThis = null;
	public JSONObject jBody;
	public String sTemperature = "";
	public String sTemperatureDetail = "";
	public String sWindSpeed = "";
	public String sWindSpeedDirection = "";
	public String sShortForecast = "";
	public String sDetailedForecast = "";
	public String sIconUrl = "";

	private HourlyForecastRunner() {
		super();
	}

	public static synchronized HourlyForecastRunner getSingleton() throws IOException {
		if (null == hfcThis) {
			hfcThis = new HourlyForecastRunner();
			hfcThis.doRun();
		}

		return hfcThis;
	}

	private void doParse() {
		JSONObject jProperties = this.jBody.getJSONObject("properties");
		JSONArray jPeriods = jProperties.getJSONArray("periods");
		JSONObject jCurrentTime = jPeriods.getJSONObject(0);

		/*
		for (int i = 0; i < jDays.length(); i++)
		{
			JSONObject jIterator = jDays.getJSONObject(i);
			sResponse += jIterator.getNumber("temperature");
			sResponse += "\r\n";
		}
		*/
		this.sTemperature = jCurrentTime.getNumber("temperature").toString();
		this.sTemperatureDetail = this.sTemperature + " F";
		this.sWindSpeed = jCurrentTime.getString("windSpeed");
		this.sWindSpeedDirection = jCurrentTime.getString("windDirection");
		this.sShortForecast = jCurrentTime.getString("shortForecast");
		this.sDetailedForecast = jCurrentTime.getString("detailedForecast");
		this.sIconUrl = jCurrentTime.getString("icon");

	}

	public void doRun() throws IOException {
		HttpClient hcClient;
		HttpRequest hrReq;
		HttpResponse<String> hrResp;

		hcClient = HttpClient.newHttpClient();

		hrReq = HttpRequest.newBuilder(
			URI.create("https://api.weather.gov/gridpoints/DTX/47,5/forecast/hourly"))
			.header("accept", "application/json")
			.GET()
			.build();

		try 
		{ hrResp = hcClient.send(hrReq, HttpResponse.BodyHandlers.ofString()); }
		catch (InterruptedException exception)
		{ 
			System.out.println("Exception in hcClient.send()");
			System.out.println(exception.toString());
			return; 
		}

		this.jBody = new JSONObject(hrResp.body().toString());

		doParse();

		return;
	}
}
