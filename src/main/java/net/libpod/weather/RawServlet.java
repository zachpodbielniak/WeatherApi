package net.libpod.weather;

import java.io.IOException;
import java.util.Enumeration;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.http.*;
import java.net.URI;
import org.json.*;

import net.libpod.weather.HourlyForecastRunner;


@WebServlet("/RawServlet")
public class RawServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;


	public RawServlet() 
	{ super(); }


	protected void doGet(
		HttpServletRequest 	request,
		HttpServletResponse 	response
	) throws ServletException, IOException
	{
		String sResponse;
		String sType;
		Enumeration<String> eParams = request.getParameterNames();
		HourlyForecastRunner hfrRunner = HourlyForecastRunner.getSingleton();

		sResponse = "Nothing was provided";
		sType = "hourlyforcast";

		/* Enumerate over query string */
		if (null != eParams)
		{
			while (eParams.hasMoreElements())
			{
				String sNextParam = eParams.nextElement();
				
				if (sNextParam.equalsIgnoreCase("type"))
				{
					sType = request.getParameter(sNextParam);
					
					if (null == sType || sType.equalsIgnoreCase(""))
					{ 
						sResponse = "Nothing was provided";
					} else if (sType.equalsIgnoreCase("hourlyforecast")) {
						response.setCharacterEncoding("application/json;charset=utf-8");
						sResponse = hfrRunner.jBody.toString();
					}
					
				}
			}
		}

		response.getWriter().append(sResponse);
	}
}
