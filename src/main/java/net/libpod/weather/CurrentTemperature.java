package net.libpod.weather;


import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import net.libpod.weather.HourlyForecastRunner;

@WebServlet("/CurrentTemperature")
public class CurrentTemperature extends HttpServlet
{
	private static final long serialVersionUID = 1L;


	public CurrentTemperature() 
	{ super(); }


	protected void doGet(
		HttpServletRequest 	request,
		HttpServletResponse 	response
	) throws ServletException, IOException
	{
		String sResponse = "";
		String sFormat = "";
		boolean bDetailed = false;
		Enumeration<String> eParams = request.getParameterNames();
		HourlyForecastRunner hfrRunner = HourlyForecastRunner.getSingleton();


		/* Enumerate over query string */
		if (null != eParams)
		{
			while (eParams.hasMoreElements())
			{
				String sNextParam = eParams.nextElement();
				
				if (sNextParam.equalsIgnoreCase("format"))
				{ sFormat = request.getParameter(sNextParam); }
				else if (sNextParam.equalsIgnoreCase("detailed"))
				{ bDetailed = Boolean.parseBoolean(request.getParameter(sNextParam)); }
			}
		}

		if (sFormat.equalsIgnoreCase("json"))
		{
			response.setContentType("application/json;charset=utf-8");
			sResponse = "{\"temperature\": ";
			sResponse += hfrRunner.sTemperature;
			sResponse += ",\"temperature-detail\": \"";
			sResponse += hfrRunner.sTemperatureDetail;
			sResponse += "\"}";
		}
		else 
		{
			response.setContentType("text/plain;charset=utf-8");
			if (true == bDetailed)
			{ sResponse = hfrRunner.sTemperatureDetail; } 
			else 
			{ sResponse = hfrRunner.sTemperature; }
		}

		response.getWriter().append(sResponse);
	}

}
