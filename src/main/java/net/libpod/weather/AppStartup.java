
package net.libpod.weather;

import java.util.Date;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import net.libpod.weather.BackgroundWorkerThread;



@WebListener
public class AppStartup implements ServletContextListener
{
	public static BackgroundWorkerThread bwtDataPoller; 
	public static Date dtInitialized;

	@Override
	public void contextDestroyed(ServletContextEvent arg0) 
	{
		System.err.println("Shutting Down!");
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) 
	{	
		System.err.println("WeatherAPI is starting up!");

		bwtDataPoller = new BackgroundWorkerThread();
		bwtDataPoller.runOnce(); /* Run Once before finishing initialization */
		bwtDataPoller.start(); /* Now start the thread */

		dtInitialized = new Date();
		System.err.println("Initialized!");
	}
}
