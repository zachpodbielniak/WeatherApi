war:
	mvn package -f pom.xml

clean:
	mvn clean -f pom.xml

container: war
	docker build -t weather-api .

tag-and-push: container
	docker tag weather-api zachpodbielniak/weather-api:latest
	docker push zachpodbielniak/weather-api:latest